
/* Scheduler includes. */
#include "SystemTasks.h"
#include "Scheduler.h"
#include "WMS.h"

using wms::WMS;

/*-----------------------------------------------------------*/
/*
 * Starts all the other tasks, then starts the scheduler.
 */
int main()
{
  ////////////////////////////////////////////////////////////////
  //  Create a Thread class that encapsulates the RTOS multi-tasking 
  //  functions.  Your Thread class should implement the 
  //  Thread-Runs-Polymorphic-Object pattern.
  //
  //  Modify your design so that your WMS class realises the 
  //  IRunnable interface.
  //
  //  Run the WMS object in its own thread of control (task).
  //
  //
  //  Create a new UI class that represents the user interface of 
  //  the system.  The UI class should realise the IRunnable interface.
  //
  //  Create your UI class and connect it to the UART class so that 
  //  the user can select wash programmes and receive output via a 
  //  console display.
  //
  //  Run the UI class in its own thread of control (task).
  //
  //
  //  Your UI class should allow the user to select a wash programme.  The 
  //  selected wash programme should be sent to the WMS class to run. 
  //
  //  Your design should allow asynchronous communication between the UI 
  //  and WMS tasks.  Use your CircularBuffer to facilitate communication.
  //
  //  Hint:
  //  You will have to come up with a strategy to deal with an 
  //  empty / full buffer.

  
  /* Start the scheduler. */
  Scheduler::start();
  
  /* We should never get here as control is now taken by the scheduler. */
  return 0;
}
