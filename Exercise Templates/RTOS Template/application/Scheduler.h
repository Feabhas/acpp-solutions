#ifndef SCHEDULER_H
#define SCHEDULER_H

#include "FreeRTOS.h"
#include "Task.h"
#include <cstdint>

using std::uint32_t;

// Simple utility wrapper around core RTOS functions
// such as starting the scheduler.
//
class Scheduler
{
public:
  static void start();
  static void sleep(std::uint32_t duration);
  static void yield();
};

#endif // SCHEDULER_H

