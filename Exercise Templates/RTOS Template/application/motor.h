#ifndef MOTOR_H
#define MOTOR_H

#include "Pin.h"

namespace wms 
{
  class Motor
  {
  public:
    enum Direction {CW, ACW};
    Motor();
    void on();
    void off();
    void changeDirection();
    
  private:
    Pin control;
    Pin direction;
  };
}

#endif // MOTOR_H