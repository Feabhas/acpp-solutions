#include "SevenSegment.h"

namespace wms 
{
  SevenSegment::SevenSegment() :
    a(Pin::PORT1, Pin::PIN16, Pin::OUT),
    b(Pin::PORT1, Pin::PIN17, Pin::OUT),
    c(Pin::PORT1, Pin::PIN18, Pin::OUT),
    d(Pin::PORT1, Pin::PIN19, Pin::OUT)
  {
    a = b = c = d = 1;
  }
  
  void SevenSegment::display(uint32_t status)
  {
    a = ((status >> 0) & 0x01);
    b = ((status >> 1) & 0x01);
    c = ((status >> 2) & 0x01);
    d = ((status >> 3) & 0x01);
  }
}
