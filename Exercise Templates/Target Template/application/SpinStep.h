#ifndef SPIN_STEP_H
#define SPIN_STEP_H

#include "MotorStep.h"

class SpinStep : public MotorStep
{
public:
  SpinStep(StepType step, SevenSegment& disp, Motor& m);
  virtual ~SpinStep() {}
  virtual void run();
};

#endif
