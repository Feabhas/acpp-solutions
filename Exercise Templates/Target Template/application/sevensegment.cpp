#include "SevenSegment.h"

SevenSegment::SevenSegment() :
  a(Pin::PORT1, Pin::PIN16, Pin::OUT),
  b(Pin::PORT1, Pin::PIN17, Pin::OUT),
  c(Pin::PORT1, Pin::PIN18, Pin::OUT),
  d(Pin::PORT1, Pin::PIN19, Pin::OUT)
{
  a.set();
  b.set();
  c.set();
  d.set();
}

void SevenSegment::display(uint32_t value)
{
  ((value >> 0) & 0x01)? a.set() : a.clear();
  ((value >> 1) & 0x01)? b.set() : b.clear();
  ((value >> 2) & 0x01)? c.set() : c.clear();
  ((value >> 3) & 0x01)? d.set() : d.clear();
}