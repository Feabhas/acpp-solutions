

class Buffer
{
public:
  enum Error {OK, FULL, EMPTY};
  
  Buffer(unsigned int sz);
  ~Buffer();

  Error add(int value);
  Error get(int& value);
  bool  isEmpty();
  void  flush();
  
  // Operator overloads
  //
  Error operator<<(int value);
  Error operator>>(int& value);
  
private:
  unsigned int read;
  unsigned int write;
  unsigned int numItems;
  const unsigned int bufferSize;
  
  // Dynamically-allocated buffer
  int* const buffer;  
};

Buffer::Error operator>>(int value, Buffer& b);
Buffer::Error operator<<(int& value, Buffer& b);