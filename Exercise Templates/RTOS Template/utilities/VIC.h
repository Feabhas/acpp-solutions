#ifndef VIC_H
#define VIC_H

#include <stdint.h>
#include <NXP/iolpc2129.h>

class VIC
{
public:
  enum interruptSource { 
    WDT = 0,
    TIMER0 = 4,
    TIMER1 = 5,
    UART0 = 6,
    UART1 = 7,
    PWM0 = 8,
    I2C = 9,
    SPI0 = 10,
    SPI1 = 11,
    PLL = 12,
    RTC = 13,
    EINT0 = 14,
    EINT1 = 15,
    EINT2 = 16,
    EINT3 = 17,
    A2D = 18};
  
  enum slotNo { slot0, slot1, slot2, slot4,
                slot5, slot6, slot7, slot8,
                slot9, slot10, slot11, slot12,
                slot13, slot14, slot15, slotGeneral};  
  
  static void configureSlot(slotNo slot, interruptSource channel, uint32_t isr);
  static void ackInt() { VICVectAddr = 0;  }
};


#endif
