#include "Motor.h"

namespace wms 
{
  Motor::Motor() : 
    control(Pin::PORT1, Pin::PIN20, Pin::OUT),
    direction(Pin::PORT1, Pin::PIN21, Pin::OUT)
  {
    control = 0;
    direction = CW;
  }
  
  void Motor::on()
  {
    control = 1;
  }
  
  void Motor::off()
  {
    control = 0;
  }
  
  void Motor::changeDirection()
  {
    direction = (direction == CW? ACW : CW);
  }
}