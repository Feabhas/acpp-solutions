#include "WashProgramme.h"
#include "Step.h"
#include <algorithm>
#include <functional>

WashProgramme::WashProgramme(Programme& wash) : 
  programme(wash)
{
}

void WashProgramme::run()
{
  std::for_each(programme.begin(), programme.end(), std::mem_fun(&Step::run));
}


