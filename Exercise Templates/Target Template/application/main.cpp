#include "SevenSegment.h"
#include "motor.h"
#include "Step.h"
#include "WashProgramme.h"

#include "SpinStep.h"
#include "WashStep.h"
#include "RinseStep.h"

#include "UART.h"

#include "MotorFailed.h"
#include "OpticalSensor.h"

#define ARRAY_SIZE(array) (sizeof(array)/sizeof(array[0]))

int main()
{
  SevenSegment the7Segment;
  Motor theMotor;             // to ensure the motor is not spinning 
  OpticalSensor os;
  
  UART com1(0xE0010000);
  
  Step empty(Step::EMPTY, the7Segment);
  Step fill(Step::FILL, the7Segment);
  Step heat(Step::HEAT, the7Segment);
  WashStep wash(Step::WASH, the7Segment, theMotor, os);
  RinseStep rinse(Step::RINSE, the7Segment, theMotor);
  SpinStep spin(Step::SPIN, the7Segment, theMotor);
  Step dry(Step::DRY, the7Segment);
  Step complete(Step::COMPLETE, the7Segment);
  
  WashProgramme::Programme colourWash;
  
  colourWash.push_back(&fill);
  colourWash.push_back(&heat);
  colourWash.push_back(&wash);
  colourWash.push_back(&empty);
  colourWash.push_back(&fill);
  colourWash.push_back(&rinse);
  colourWash.push_back(&empty);
  colourWash.push_back(&spin);
  colourWash.push_back(&dry);
  colourWash.push_back(&complete);
  
  WashProgramme colourWashProgramme(colourWash);
  
  try {
    com1<< "Running colour wash...\r\n";
    colourWashProgramme.run();
    com1 << "Colour wash complete..." << endl;
  }
  catch(const MotorFailed& e)
  {
    com1 << e.what();
  }
  
  while(true){
    char ch;
    com1 >> ch;
    com1 << ch;
  }
  
  return 0;
  
}
