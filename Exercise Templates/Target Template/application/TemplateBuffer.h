#ifndef TEMPLATEBUFFER_H
#define TEMPLATEBUFFER_H

#include <cstdint>

using namespace std;

template <typename T = int, uint32_t sz = 8>
class CircularBuffer
{
public:
  enum Error {OK, FULL, EMPTY};
  
  CircularBuffer();
  ~CircularBuffer();

  Error add(int value);
  Error get(int& value);
  bool isEmpty();
  void flush();
  
  // Operator overloads
  //
  void operator<<(int value);
  void operator>>(int& value);
  
private:
  unsigned int read;
  unsigned int write;
  unsigned int numItems;
  
  // Dynamically-allocated buffer
  T* const buffer;  
};


template <typename T, uint32_t sz>
CircularBuffer<T, sz>::CircularBuffer() : buffer(new T[sz])
{
  flush();
}

template <typename T, uint32_t sz>
CircularBuffer<T, sz>::~CircularBuffer()
{
  delete[] buffer;
}

template <typename T, uint32_t sz>
CircularBuffer<T, sz>::Error CircularBuffer<T, sz>::add(int value)
{
  if (numItems == sz)
  {
    return FULL;
  }
  else
  {
    buffer[write] = value;
    
    ++numItems;
    ++write;
    if (write == sz) write = 0;
    
    return OK;
  }
}

template <typename T, uint32_t sz>
CircularBuffer<T, sz>::Error CircularBuffer<T, sz>::get(int& value)
{
  if (numItems == 0)
  {
    return EMPTY;
  }
  else
  {
    value = buffer[read];
    
    --numItems;
    ++read;
    if (read == sz) read = 0;
    
    return OK;
  }
}

template <typename T, uint32_t sz>
bool  CircularBuffer<T, sz>::isEmpty()
{
  return (numItems == 0);
}

template <typename T, uint32_t sz>
void  CircularBuffer<T, sz>::flush()
{
  read = 0;
  write = 0;
  numItems = 0;
}

template <typename T, uint32_t sz>
void CircularBuffer<T, sz>::operator<<(int value)
{
  return add(value);
}

template <typename T, uint32_t sz>
void CircularBuffer<T, sz>::operator>>(int& value)
{
  return get(value);
}

template <typename T, uint32_t sz>
void operator>>(int value, CircularBuffer<T, sz>& b)
{
  return b.add(value);
}

template <typename T, uint32_t sz>
void operator<<(int& value, CircularBuffer<T, sz>& b)
{
  return b.get(value);
}

#endif // TEMPLATEBUFFER_H