#include "OpticalSensor.h"

OpticalSensor::OpticalSensor() :
  feedback(Pin::PORT0, Pin::PIN22, Pin::IN)
{
}

bool OpticalSensor::read()
{
  return feedback.isSet();
}

OpticalSensor::operator bool()
{
  return feedback.isSet();
}