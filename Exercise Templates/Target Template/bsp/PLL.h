#ifndef __PLL_H__
#define __PLL_H__
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif
  
void initPLL(void);
uint32_t getprocessorClockFreq(void);
uint32_t getperipheralClockFreq(void);

#ifdef __cplusplus
}
#endif


#endif
