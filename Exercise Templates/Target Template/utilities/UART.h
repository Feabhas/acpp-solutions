//
// SERIAL IO
//
// Copyright Feabhas Ltd 2008
// -----------------------------------------------------------------------------

// UART.h
// Declaration of serial UART class

#ifndef UARTH
#define UARTH


#include <stdint.h>
#include "TemplateBuffer.h"

class UART
{
public:
  enum baudRate { b9600 = 9600, b38400 = 38400, b115k = 115200};
  enum dataBits {five = 0, six = 1, seven = 2, eight = 3};
  enum stopBits { none = 0, one = (1<<2) };
  enum parity { off = 0, odd = (1<<3), even = (3<<3)};
  enum interruptEnable { rxEnable = 0x01, txEnable = 0x10 };
  enum FIFOcontrol { FIFOEnable = 0x01, rxFIFOReset = 0x02, txFIFOReset = 0x04 };
    
  explicit UART ( unsigned long address, 
                  baudRate baud = b9600, 
                  dataBits db = eight,
                  stopBits sb = none,
                  parity pb = off);
    
  unsigned char read () const;
  void write (char);
  void write (const char*);
  
  // Operator overloads
  //
  UART& operator<<(char ch);
  UART& operator<<(const char* str);
  void   operator>>(char& ch);
  UART& operator<< (UART& (*pf)(UART& os));
  
private:
  // Interface realisation
  //
  virtual void display(uint32_t status);
  virtual uint32_t get();
 
  // Implementation:
  //
  enum
  { 
    DLAB_BIT = 0x80,
    RDR_FLAG = 0x01,
    THRE_FLAG = 0x20
  };

  volatile uint8_t * const pRBR;
  volatile uint8_t * const pTHR;
  volatile uint8_t * const pDLL;
  volatile uint8_t * const pDLM;
  volatile uint8_t * const pLCR;
  volatile uint8_t * const pLSR;
  volatile uint8_t * const pFCR;
  volatile uint8_t * const pIER;
  volatile uint8_t * const pIIR;

  static UART* self;
  static __arm __irq void ISR();
  mutable CircularBuffer<unsigned char> buffer;  

  UART(const UART&);
  UART& operator=(const UART&);
};

UART& endl(UART& uart);

#endif // UARTH
