// Circular Buffer header file
#ifndef CIRCULAR_BUFFER_H
#define CIRCULAR_BUFFER_H

class CircularBuffer
{
public:
  enum Error {OK, FULL, EMPTY};
  
  CircularBuffer();
  Error add(int value);
  Error get(int& value);
  bool isEmpty();
  
private:
  unsigned int write;
  unsigned int read;
  unsigned int numItems;
  
  static const unsigned int buffer_size = 8;
  int buffer[buffer_size];
};

#endif
