//
// SERIAL IO
//
// Copyright Feabhas Ltd 2008
// -----------------------------------------------------------------------------

// UART.h
// Declaration of serial UART class

#ifndef UART_H
#define UART_H

#include "UI_Interfaces.h"
#include <stdint.h>

namespace LPC2129
{
  using wms::I_Input;
  using wms::I_Output;
  
  class UART : public I_Input, public I_Output
  {
  public:
    enum baseAddress { UART0 = 0xE000C000, UART1 = 0xE0010000 };
    enum baudRate { b9600 = 9600, b38400 = 38400, b115k = 115200};
    enum dataBits {five = 0, six = 1, seven = 2, eight = 3};
    enum stopBits { none = 0, one = (1<<2) };
    enum parity { off = 0, odd = (1<<3), even = (3<<3)};
      
    explicit UART (unsigned long address = UART1, 
                    baudRate baud = b9600, 
                    dataBits db = eight,
                    stopBits sb = none,
                    parity pb = off);
      
    unsigned char read () const;
    void write (char);
    void write (const char*);
    
    // Operator overloads
    //
    UART& operator<<(char ch);
    UART& operator<<(const char* str);
    void   operator>>(unsigned char& ch);
    UART& operator<< (UART& (*pf)(UART& os));
    
  private:
    // Interface realisation
    //
    virtual void display(uint32_t status);
    virtual uint32_t get();
    
    // Implementation:
    //
    enum
    { 
      DLAB_BIT = 0x80,
      RDR_FLAG = 0x01,
      THRE_FLAG = 0x20
    };
  
    volatile uint8_t * const pRBR;
    volatile uint8_t * const pTHR;
    volatile uint8_t * const pDLL;
    volatile uint8_t * const pDLM;
    volatile uint8_t * const pLCR;
    volatile uint8_t * const pLSR;  
  
    UART(const UART&);
    UART& operator=(const UART&);
  };
  
  // --------------------------------------
  // endl function
  //
  UART& endl(UART& uart);
} 

#endif // UART_H
