#include "VIC.h"
#include <intrinsics.h>

void VIC::configureSlot(slotNo slot, interruptSource channel, uint32_t isr)
{
 __disable_interrupt ();
  // this next statement treats the vector address as an array and indexes in based on channel no.
  *(static_cast<unsigned long volatile*>(&VICVectAddr0) + slot) = isr;
  *(static_cast<unsigned long volatile*>(&VICVectCntl0) + slot) = 0x20 | channel;  // Map channel to slot
  VICIntEnable = (1 << channel);  // Enable channel 
  VICVectAddr = 0;
  __enable_interrupt ();
}
