// -----------------------------------------------------------------------------
// SERIAL IO
//
// Copyright Feabhas Ltd 2008
// -----------------------------------------------------------------------------

// UART.cpp
// Definition of serial UART class

#include "UART.h"
#include "PLL.h"
#include "VIC.h"

#define array_sizeof(a) (sizeof(a))/(sizeof(a[0]))

//-----------------------------------------------------------------------------
// Static call-back pointer
//
UART* UART::self = 0;


UART::UART (unsigned long address,
          baudRate baud,
          dataBits db,
          stopBits sb,
          parity pb)
  : pRBR (reinterpret_cast<unsigned char*>(address)),
    pTHR (pRBR), pDLL (pRBR),
    pDLM (reinterpret_cast<unsigned char*>(address + 0x04)),
    pLCR (reinterpret_cast<unsigned char*>(address + 0x0C)),
    pLSR (reinterpret_cast<unsigned char*>(address + 0x14)),
    pFCR (reinterpret_cast<unsigned char*>(address + 0x08)),
    pIER (reinterpret_cast<unsigned char*>(address + 0x04)),
    pIIR (reinterpret_cast<unsigned char*>(address + 0x08))
{
  self = this;
  
  const unsigned short VPD_divider = getperipheralClockFreq () / (16 * baud);
      
  *pLCR |= db;       // 8 bits
  *pLCR |= sb;       // 1 stop bit
  *pLCR |= pb;       // no parity
      
  *pLCR |= DLAB_BIT;  // DLAB bit set
  *pDLL = static_cast<unsigned char>(VPD_divider);        // LSB
  *pDLM = static_cast<unsigned char>(VPD_divider >> 8); // MSB
  *pLCR &= ~DLAB_BIT; // DLAB bit clear
  
  // Configure the VIC to interrupt on Rx
  //
  *pFCR = (FIFOEnable | rxFIFOReset | txFIFOReset);
  
  VIC::configureSlot(VIC::slot1, VIC::UART1, reinterpret_cast<uint32_t>(&UART::ISR));
  
  *pIER = rxEnable;
}


unsigned char UART::read () const
{
  int ch;
  while (buffer.isEmpty())
  {
    // wait until there's some data...
  }
  buffer.get(ch);
  return ch;
}
  

void UART::write (char c)
{
  while (!(*pLSR & THRE_FLAG))
  {
    ; // wait...
  }
  *pTHR = static_cast<unsigned char>(c);
}


void UART::write (const char* str)
{
  while (*str != '\0')
  {
    write (*str);
    str++;
  }
}

//-----------------------------------------------------------------------------
// Interrupt service routine

__irq __arm
void UART::ISR()
{
  uint8_t IIR_read;

  // 1.  Read the Rx buffer and store 
  self->buffer.add(*(self->pRBR));
  
  // 2.  Acknowledge the interrupt at the VIC. 
  VIC::ackInt();
  
  // 3.  Acknowledge the interrupt at the UART. 
  IIR_read = *self->pIIR;
}

// ---------------------------------------------------------------
// Operator overloads
//
UART& UART::operator<<(char ch)
{
  write(ch);
  if(ch == '\r') {
    write('\n');
  }
  return *this;
}

UART& UART::operator<<(const char* str)
{
  write(str);
  return *this;
}

void UART::operator>>(char& ch)
{
  ch = read();
}

UART& UART::operator<< (UART& (*pf)(UART& uart))
{
  return pf(*this);
}

UART& endl(UART& uart)
{
  uart << "\n\r";
  return uart;
}

// ---------------------------------------------------------------
// Interface realisation
//

static char* stepNames[] = 
{
  "Empting",
  "Filling",
  "Heating",
  "Washing",
  "Rinsing",
  "Spinning",
  "Drying",
  "DONE!"
};

void UART::display(uint32_t status)
{
  // NOTE: Requires knowledge of the wash
  // programmes.
  //
  if (status > array_sizeof(stepNames)) 
  {
    status = array_sizeof(stepNames); // That is, an unknown wash...
  }
  
  *this << stepNames[status] << endl; // Using UART's own endl (NOT std::endl!)
}


uint32_t UART::get()
{
  return read();
}

