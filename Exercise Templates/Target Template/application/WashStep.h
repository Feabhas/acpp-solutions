#ifndef WASH_STEP_H
#define WASH_STEP_H

#include "MotorStepException.h"

class WashStep : public MotorStepException
{
public:
  WashStep(StepType step, SevenSegment& disp, Motor& m, OpticalSensor& os);
  virtual ~WashStep() {}
  virtual void run();
};

#endif
