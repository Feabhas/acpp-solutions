#include "Scheduler.h"

#include "FreeRTOS.h"
#include "task.h"

void Scheduler::start()
{
  vTaskStartScheduler();
}

void Scheduler::sleep(std::uint32_t duration)
{
  portTickType delay = static_cast<portTickType>(duration) / portTICK_RATE_MS;
  vTaskDelay(delay);
}

void Scheduler::yield()
{
  sleep(0);
  //taskYIELD();
}