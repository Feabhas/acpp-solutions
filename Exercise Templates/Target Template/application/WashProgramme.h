#ifndef WASHPROGRAMME_H
#define WASHPROGRAMME_H

#include <cstddef>
#include <list>

class Step;

class WashProgramme
{
public:
  typedef std::list<Step*> Programme;
  WashProgramme(Programme& wash);
  void run();
  
private:
  Programme programme;
};

#endif // WASHPROGRAMME_H