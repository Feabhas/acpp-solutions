#include "Motor.h"

Motor::Motor() : 
  control(Pin::PORT1, Pin::PIN20, Pin::OUT),
  direction(Pin::PORT1, Pin::PIN21, Pin::OUT)
{
  control.clear();
  direction.clear();
  changeDirection();
}

void Motor::on()
{
  control.set();
}

void Motor::off()
{
  control.clear();
}

bool Motor::isOn()
{
  return control.isSet();
}

void Motor::changeDirection()
{
  if(direction.isSet())
  {
    direction.clear();
  }
  else
  {
    direction.set();
  }
}