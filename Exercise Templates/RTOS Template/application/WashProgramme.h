#ifndef WASHPROGRAMME_H
#define WASHPROGRAMME_H

#include "Step.h"
#include "WashProgramme.h"
#include <cstddef>
#include <vector>

namespace wms 
{
  using std::vector;
  
  enum ProgrammeType 
  {
    WHITE, 
    COLOUR, 
    MIXED, 
    ECONOMY, 
    PROGRAM1, 
    PROGRAM2,
    NUMBER_OF_PROGRAMMES
  };
  
  
  class WashProgramme
  {
  public:
    typedef vector<Step*> Programme;
    WashProgramme(Programme& wash);
    void run();
    
  private:
    Programme programme;
  };
  
  
  class WashProgrammeFactory
  {
  public:  
    WashProgrammeFactory(StepFactory* stepFactory);
    ~WashProgrammeFactory();
    WashProgramme* make(ProgrammeType prog);
      
  private:
    vector<WashProgramme*> programmeContainer;
    StepFactory* pStepFactory;
  };
}

#endif // WASHPROGRAMME_H