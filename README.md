Set of solutions for the AC++ course with tagged commits, e.g.
Ex1
Ex10
Ex2
Ex3.1
Ex3.2
Ex4.2
Ex4.3
Ex4.4
Ex5
Ex6
Ex7
Ex8
Ex9

To look at a particular solution then create a branch based on that Tag, based on:
git checkout -b <new_branch_name> <tag>

e.g.
git checkout -b exercise1 Ex1

will create a new branch 'exercise1' based on commit Tag Ex1
