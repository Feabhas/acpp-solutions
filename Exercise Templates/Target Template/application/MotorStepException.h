#ifndef EX_MOTOR_STEP_H
#define EX_MOTOR_STEP_H
// ---------------------------------------------------------
// ABC for exception motor-based steps.
//
#include "MotorStep.h"
class OpticalSensor;

class MotorStepException : public MotorStep
{
public:
  MotorStepException(StepType step, SevenSegment& disp, Motor& m, OpticalSensor& os) : MotorStep(step, disp, m), feedbackSensor(os) {}
protected:
  void checkMotorIsRunning();
private:
  OpticalSensor& feedbackSensor;
};

#endif

