#ifndef WMS_H
#define WMS_H

#include "SevenSegment.h"
#include "Step.h"
#include "WashProgramme.h"
#include "Motor.h"
#include "UART.h"

namespace wms 
{
  class WMS
  {
  public:  
    WMS();
    void runWash(ProgrammeType wash);
    
  private:
    SevenSegment         sevenSeg;
    LPC2129::UART       console;
    Motor                motor;
    StepFactory          stepFactory;
    WashProgrammeFactory washProgrammeFactory;
  };
}

#endif // WMS_H