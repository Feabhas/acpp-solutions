#include "Pin.h"

namespace wms 
{
  Pin::Pin(Port p, PinNumber n, Direction d) :
    registers(*(reinterpret_cast<Registers*>(p))),
    num(n)
  {
    registers.DIR |= (d << num);
    registers.CLR  = (1 << num);   
  }
  
  void Pin::set()
  {
    registers.SET = (1 << num);
  }
  
  void Pin::clear()
  {
    registers.CLR = (1 << num);
  }
  
  bool Pin::isSet() const
  {
    return ((registers.PIN & (1 << num)) != 0);
  }
  
  // -----------------------------------------------------
  // Operator overloads
  //
  Pin& Pin::operator=(uint32_t rhs)
  {
    if (rhs == 0)
    {
      clear();
    }
    else
    {
      set();
    }
    
    return *this;
  }
  
  Pin& Pin::operator=(const Pin& rhs)
  {
    if (rhs.isSet())
    {
      set();
    }
    else
    {
      clear();
    }
    
    return *this;
  }
  
  
  Pin::operator int()
  {
    return (isSet()? 1 : 0);
  }
}