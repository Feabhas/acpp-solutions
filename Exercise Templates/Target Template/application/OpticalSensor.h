#ifndef OPTICALSENSOR_H
#define OPTICALSENSOR_H

#include "Pin.h"

class OpticalSensor
{
public:
  OpticalSensor();
  bool read();
  operator bool();
  
private:
  Pin feedback;
};


#endif // OPTICALSENSOR_H