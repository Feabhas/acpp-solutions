// Buffer implementation

#include "Buffer.h"

Buffer::Buffer(unsigned int sz) :
  bufferSize(sz),
  buffer(new int[sz])
{
  flush();
}
  
Buffer::~Buffer()
{
  delete[] buffer;
}

Buffer::Error Buffer::add(int value)
{
  if (numItems == bufferSize)
  {
    return Buffer::FULL;
  }
  else
  {
    buffer[write] = value;
    
    ++numItems;
    ++write;
    if (write == bufferSize) write = 0;
    return Buffer::OK;
  }
}

Buffer::Error Buffer::get(int& value)
{
  if (numItems == 0)
  {
    return Buffer::EMPTY;
  }
  else
  {
    value = buffer[read];
    
    --numItems;
    ++read;
    if (read == bufferSize) read = 0;
    return Buffer::OK;
  }
}

bool  Buffer::isEmpty()
{
  return (numItems == 0);
}

void  Buffer::flush()
{
  read = 0;
  write = 0;
  numItems = 0;
}

Buffer::Error Buffer::operator<<(int value)
{
  return add(value);
}

Buffer::Error Buffer::operator>>(int& value)
{
  return get(value);
}

Buffer::Error operator>>(int value, Buffer& b)
{
  return b.add(value);
}

Buffer::Error operator<<(int& value, Buffer& b)
{
  return b.get(value);
}