// TIPO.cpp
//
// Implementation of Thread-Is-Polymorphic-Object


#include "TIPO.h"
#include "FreeRTOS.h"
#include "task.h"


Thread::Thread(Thread::Priority prio) : 
  stack(Thread::DefaultStack),
  priority (prio),
  created(false)
{ 
}

Thread::~Thread()
{
  if (created)
  {
    vTaskDelete(handle);
  }
}

void Thread::start()
{
  portBASE_TYPE error;
  
  error = xTaskCreate(&Thread::scheduledFunction, 
                 "TIPO", 
                 static_cast<portSHORT>(stack), 
                 static_cast<void*>(this),
                 priority,
                 &handle);
  
  if(error == pdPASS)
  {
    created = true;
  }
  else
  {
    // Couldn't allocate enough memory for 
    // the task; hence fail.
    created = false;
  }
}
  

void Thread::scheduledFunction(void* threadArg)
{
  Thread& self = *(reinterpret_cast<Thread*>(threadArg));
  self.run();
}


///////////////////////////////////////////////

