/**************************************************
 *
 * This module contains the function `__low_level_init', a function
 * that is called before the `main' function of the program.  Normally
 * low-level initializations - such as setting the prefered interrupt
 * level or setting the watchdog - can be performed here.
 *
 * Note that this function is called before the data segments are
 * initialized, this means that this function cannot rely on the
 * values of global or static variables.
 *
 * When this function returns zero, the startup code will inhibit the
 * initialization of the data segments. The result is faster startup,
 * the drawback is that neither global nor static data will be
 * initialized.
 *
 * Copyright 1999-2004 IAR Systems. All rights reserved.
 *
 * $Revision: 21623 $
 *
 **************************************************/
#include <stdint.h>
#include <intrinsics.h>
#include <NXP/iolpc2129.h>
#include "PLL.h"

#ifdef __cplusplus
extern "C" {
#endif

#pragma language=extended
  
void CPUinit(void);
void INTERRUPTSinit(void);

__irq __arm void IRQ_Handler(void);
__interwork int __low_level_init(void);


// Constants to setup the MAM.
// If we are using FreeRTOS we must configure the MAM
// settings differently.
//
#define FreeRTOS_MAM_MODE     ((unsigned char) 0x02)
#define FreeRTOS_MAM_CYCLES   ((unsigned char) 0x03)

#define NO_RTOS_MAM_MODE      ((unsigned char) 0x02)
#define NO_RTOS_MAM_CYCLES    ((unsigned char) 0x04)

#ifdef FREE_RTOS
  #define MAM_MODE    FreeRTOS_MAM_MODE
  #define MAM_CYCLES  FreeRTOS_MAM_CYCLES

#else
  #define MAM_MODE    NO_RTOS_MAM_MODE
  #define MAM_CYCLES  NO_RTOS_MAM_CYCLES

#endif


/*-----------------------------------------------------------*/
__interwork int __low_level_init(void)
{
  /*==================================*/
  /*  Initialize hardware.            */
  /*==================================*/
  //SetupHardware();
  CPUinit();
  
  /*==================================*/
  /* Choose if segment initialization */
  /* should be done or not.           */
  /* Return: 0 to omit seg_init       */
  /*         1 to run seg_init        */
  /*==================================*/
  return 1;
}



/*-------------------------------------------------------------------------------------------------------------------------------*/
static void DefDummyInterrupt()
{
}


/*-------------------------------------------------------------------------------------------------------------------------------*/
void CPUinit(void)
{
  initPLL();

  //Init MAM & Flash memory fetch
  MAMCR_bit.MODECTRL = MAM_MODE;    // setup MAM mode.
  MAMTIM_bit.CYCLES =  MAM_CYCLES;  // setup fetch cycles

  //GPIO init
  PINSEL0=0x00050005; //P0 enable UART0 and UART1 Tx/Rx pins
  PINSEL1=0;          //P0 upper 16 bits all GPIO
}


/*-------------------------------------------------------------------------------------------------------------------------------*/
//INTERRUPT initialisation and handling functions
/*-------------------------------------------------------------------------------------------------------------------------------*/
void INTERRUPTSinit(void)
{
#ifdef SRAM_VIA_JLINK
  MEMMAP = 2;   // remap IVT to RAM
#endif
  
  // Setup interrupt controller.
  VICProtection = 0;

  // Disable ALL interrupts
  VICIntEnClear = 0xffffffff;
  VICDefVectAddr = (uint32_t)&DefDummyInterrupt;
}


#pragma language=default

#ifdef __cplusplus
}
#endif
