// -----------------------------------------------------------------------------
// SERIAL IO
//
// Copyright Feabhas Ltd 2008
// -----------------------------------------------------------------------------

// UART.cpp
// Definition of serial UART class

#include "UART.h"
#include "PLL.h"

#define array_sizeof(a) (sizeof(a))/(sizeof(a[0]))

namespace LPC2129
{

  UART::UART (unsigned long address,
            baudRate baud,
            dataBits db,
            stopBits sb,
            parity pb)
    : pRBR (reinterpret_cast<unsigned char*>(address)),
      pTHR (pRBR), pDLL (pRBR),
      pDLM (reinterpret_cast<unsigned char*>(address + 0x04)),
      pLCR (reinterpret_cast<unsigned char*>(address + 0x0C)),
      pLSR (reinterpret_cast<unsigned char*>(address + 0x14))
  {
    // There's an issue with this FreeRTOS project that
    // means the getperipheralClockFreq() function doesn't
    // always return the correct frequency.
    // For expidition's sake I've hard-coded the divider
    // value, based on pclk = cclk = 60MHz
    // Obviously, this needs resolving.
    //
    //const unsigned short VPD_divider = getperipheralClockFreq() / (16 * baud);
    const unsigned short VPD_divider = 60000000 / (16 * baud);
    
    *pLCR |= db;       // 8 bits
    *pLCR |= sb;       // 1 stop bit
    *pLCR |= pb;       // no parity
        
    *pLCR |= DLAB_BIT;  // DLAB bit set
    *pDLL = static_cast<unsigned char>(VPD_divider);        // LSB
    *pDLM = static_cast<unsigned char>(VPD_divider >> 8); // MSB
    *pLCR &= ~DLAB_BIT; // DLAB bit clear
  }
  
  
  unsigned char UART::read () const
  {
    while (!(*pLSR & RDR_FLAG))
    {
      ; // wait...
    }
    return *pRBR;
  }
    
  
  void UART::write (char c)
  {
    while (!(*pLSR & THRE_FLAG))
    {
      ; // wait...
    }
    *pTHR = static_cast<unsigned char>(c);
  }
  
  
  void UART::write (const char* str)
  { 
    while (*str != '\0')
    {
      write (*str);
      ++str;
    }
  }
  
  // ---------------------------------------------------------------
  // Operator overloads
  //
  UART& UART::operator<<(char ch)
  {
    write(ch);
    return *this;
  }
  
  UART& UART::operator<<(const char* str)
  {
    write(str);
    return *this;
  }
  
  void UART::operator>>(unsigned char& ch)
  {
    ch = read();
  }
  
  UART& UART::operator<< (UART& (*pf)(UART& uart))
  {
    return pf(*this);
  }
  
  UART& endl(UART& uart)
  {
    uart << "\n\r";
    return uart;
  }
  
  // ---------------------------------------------------------------
  // Interface realisation
  //
  
  static char* stepNames[] = 
  {
    "Empting",
    "Filling",
    "Heating",
    "Washing",
    "Rinsing",
    "Spinning",
    "Drying",
    "DONE!"
  };
  
  void UART::display(uint32_t status)
  {
    // NOTE: Requires knowledge of the wash
    // programmes.
    //
    if (status > array_sizeof(stepNames)) 
    {
      status = array_sizeof(stepNames); // That is, an unknown wash...
    }
    
    *this << stepNames[status - 1] << endl; // Using UART's own endl (NOT std::endl!)
  }
  
  
  uint32_t UART::get()
  {
    return read();
  }
}