// -----------------------------------------------------------------------------
// INTERRUPTS
//
// Copyright Feabhas Ltd 2008
// -----------------------------------------------------------------------------

// Timer.cpp
// Definition of Timer class
#include "Timer.h"
#include <NXP/iolpc2129.h>
#include <intrinsics.h>
#include "VIC.h"
#include "PLL.h"


Timer* Timer::stp = 0;


Timer::Timer (uint32_t address, uint32_t ms): 
  pRegs (reinterpret_cast<regs*>(address))
{
  // Set up the static member stp to point to this
  // so that when the (static) interrupt function
  // executes the code can get access to the current
  // Timer object (*this)
  //
  stp = this;
  
  // Configure the hardware timer
  pRegs->IR = 1; // Clear timer interrupt flag
  
  stop();

  pRegs->MR0 = getperipheralClockFreq() / 1000 * ms; 
  pRegs->MCR = 3;

  VIC::configureSlot(VIC::slot0,
                     VIC::TIMER0, 
                     reinterpret_cast<uint32_t>(&Timer::ISR));
};


__irq __arm
void Timer::ISR()
{
  //---------------------------------------
  // TO DO:
  // Do the ISR work here...
  //


  //---------------------------------------

  // These must be the last instructions in 
  // the ISR.
  //
  stp->pRegs->IR = 1; // Clear timer interrupt flag
  VIC::ackInt();      // Acknowledge interrupt  
}

void Timer::start()
{
  pRegs->TCR = 1;
}

void Timer::stop()
{
  pRegs->TCR = 0;
}