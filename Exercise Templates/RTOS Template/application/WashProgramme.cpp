#include "WashProgramme.h"
#include "Step.h"
#include <algorithm>
#include <functional>

namespace wms 
{
  WashProgramme::WashProgramme(Programme& wash) : 
    programme(wash)
  {
  }
  
  
  void WashProgramme::run()
  {
    for_each(programme.begin(), programme.end(), std::mem_fun(&Step::run));
  }
  
  
  // ---------------------------------------------------------
  
  WashProgrammeFactory::WashProgrammeFactory(StepFactory* stepFactory) : 
    programmeContainer(NUMBER_OF_PROGRAMMES),
    pStepFactory(stepFactory)
  {
    fill(programmeContainer.begin(), 
         programmeContainer.end(),
         static_cast<WashProgramme*>(0));
  }
  
  
  WashProgramme* WashProgrammeFactory::make(ProgrammeType prog)
  {
    WashProgramme::Programme* pNewProgramme;
    
    switch(prog)
    {
    case WHITE:
      if(programmeContainer[prog] == 0)
      {  
        pNewProgramme = new WashProgramme::Programme;
        pNewProgramme->push_back(pStepFactory->make(FILL));
        pNewProgramme->push_back(pStepFactory->make(WASH));
        pNewProgramme->push_back(pStepFactory->make(EMPTY));
        pNewProgramme->push_back(pStepFactory->make(FILL));
        pNewProgramme->push_back(pStepFactory->make(WASH));
        pNewProgramme->push_back(pStepFactory->make(RINSE));
        pNewProgramme->push_back(pStepFactory->make(EMPTY));
        pNewProgramme->push_back(pStepFactory->make(SPIN));
        pNewProgramme->push_back(pStepFactory->make(DRY));
        pNewProgramme->push_back(pStepFactory->make(COMPLETE));
        
        programmeContainer[prog] = new WashProgramme(*pNewProgramme);
      }
      break;
      
    case COLOUR:
      if(programmeContainer[prog] == 0)
      {  
        pNewProgramme = new WashProgramme::Programme;
        pNewProgramme->push_back(pStepFactory->make(FILL));
        pNewProgramme->push_back(pStepFactory->make(WASH));
        pNewProgramme->push_back(pStepFactory->make(RINSE));
        pNewProgramme->push_back(pStepFactory->make(EMPTY));
        pNewProgramme->push_back(pStepFactory->make(SPIN));
        pNewProgramme->push_back(pStepFactory->make(DRY));
        pNewProgramme->push_back(pStepFactory->make(COMPLETE));
        
        programmeContainer[prog] = new WashProgramme(*pNewProgramme);
      }
      break;
      
    default:
      if(programmeContainer[prog] == 0)
      {  
        pNewProgramme = new WashProgramme::Programme;
        pNewProgramme->push_back(pStepFactory->make(COMPLETE));
        
        programmeContainer[prog] = new WashProgramme(*pNewProgramme);
      }
      break;
    }
    return programmeContainer[prog];
  }
  
  
  WashProgrammeFactory::~WashProgrammeFactory()
  {
    for (int i = 0; i < NUMBER_OF_PROGRAMMES; ++i)
    {
      delete programmeContainer[i];
    }
  }
}