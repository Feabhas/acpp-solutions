#include "WashStep.h"
#include "wait.h"

WashStep::WashStep(StepType step, SevenSegment& disp, Motor& m, OpticalSensor& os) : 
  MotorStepException(step, disp, m, os)
{
}


void WashStep::run()
{
  Step::run();  // Display value on SevenSegment
  motor.on();
  checkMotorIsRunning();
  wait();
  motor.changeDirection();
  wait();
  motor.off();
  motor.changeDirection();
}
