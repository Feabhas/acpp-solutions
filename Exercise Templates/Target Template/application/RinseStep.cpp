#include "RinseStep.h"
#include "wait.h"

RinseStep::RinseStep(StepType step, SevenSegment& disp, Motor& m) : 
  MotorStep(step, disp, m)
{
}


void RinseStep::run()
{
  Step::run();  // Display value on SevenSegment
  motor.on();
  wait();
  wait();
  motor.off();
  motor.changeDirection();
}
