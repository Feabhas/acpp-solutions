#include "WMS.h"

namespace wms 
{
  WMS::WMS() :
    sevenSeg(),
    console(),
    motor(),
    stepFactory(&console, &motor),
    washProgrammeFactory(&stepFactory)
  {
  }
  
  void WMS::runWash(ProgrammeType wash)
  {
    WashProgramme* prog;
    switch(wash)
    {
    case WHITE:
      prog = washProgrammeFactory.make(WHITE);
      break;
      
    case COLOUR:
      prog = washProgrammeFactory.make(COLOUR);
      break;
      
    default:
      prog = washProgrammeFactory.make(ECONOMY);
      break;
    }
    
    if(prog != 0)
    {
      prog->run();
    }
  }
}