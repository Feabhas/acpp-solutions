#ifndef UI_INTERFACES_H
#define UI_INTERFACES_H

#include <cstdint>

namespace wms 
{
  using std::uint32_t;
  
  // For devices displaying system
  // status information to the user
  //
  class I_Output
  {
  public:
    virtual void display(uint32_t status) = 0;
  };
  
  
  // For devices receiving commands
  // from the user.
  //
  class I_Input
  {
  public:
    virtual uint32_t get() = 0;
  };
}  
#endif // UI_INTERFACES_H