#ifndef RINSE_STEP_H
#define RINSE_STEP_H

#include "MotorStep.h"

class RinseStep : public MotorStep
{
public:
  RinseStep(StepType step, SevenSegment& disp, Motor& m);
  virtual ~RinseStep() {}
  virtual void run();
};

#endif
