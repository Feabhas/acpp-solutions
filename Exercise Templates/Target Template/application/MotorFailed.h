#ifndef MOTOR_FAILED_H
#define MOTOR_FAILED_H


// Exception for failed motor
class MotorFailed
{
public:
  MotorFailed(int err):errorCode(err){}
  const char* what() const;
private:
  const int errorCode;
};


#endif // MOTOR_FAILED_H