#ifndef MOTOR_H
#define MOTOR_H

#include "Pin.h"

class Motor
{
public:
  Motor();
  void on();
  void off();
  void changeDirection();
  bool isOn();
private:
  Pin control;
  Pin direction;
};


#endif // MOTOR_H