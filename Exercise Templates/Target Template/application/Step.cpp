#include "Step.h"
#include "SevenSegment.h"
#include "wait.h"
#include "UART.h"

Step::Step(StepType step, SevenSegment& disp) : 
  pDisplay(disp),
  stepNumber(step)
{
}

void Step::run()
{
  pDisplay.display(stepNumber + 1);  // Since StepTypes are zero-based
  wait();
}

