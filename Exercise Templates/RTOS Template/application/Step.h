#ifndef STEP_H
#define STEP_H

#include <vector>
namespace wms 
{
  using std::vector;

  class SevenSegment;
  class StepFactory;
  class Motor;
  class I_Output;
  
  enum StepType
    {
      EMPTY,
      FILL,
      HEAT,
      WASH,
      RINSE,
      SPIN,
      DRY,
      COMPLETE,
      NUMBER_OF_STEPS
    };
  
  class Step
  {
  public:
    virtual void run();
    virtual ~Step() {}
  
  protected:
    Step(StepType step, I_Output* output);
    
  private:
    I_Output* pOutput;
    StepType stepNumber;
    
    friend class StepFactory;
  };
  
  // ---------------------------------------------------------
  // ABC for motor-based steps.
  //
  class MotorStep : public Step
  {
  public:
    MotorStep(StepType step, I_Output* output, Motor* m) : Step(step, output), motor(m) {}
    virtual ~MotorStep() {}
    virtual void run() = 0;
    
  protected:
    Motor* motor;
  };
  
  
  class WashStep : public MotorStep
  {
  public:
    WashStep(StepType step, I_Output* output, Motor* m);
    virtual ~WashStep() {}
    virtual void run();
  };
  
  
  class SpinStep : public MotorStep
  {
  public:
    SpinStep(StepType step, I_Output* output, Motor* m);
    virtual ~SpinStep() {}
    virtual void run();
  };
  
  
  // ---------------------------------------------------------
  class StepFactory
  {
  public:  
    StepFactory(I_Output* output, Motor* motor);
    ~StepFactory();
    Step* make(StepType step);
      
  private:
    vector<Step*> stepContainer;
    I_Output* pOutput;
    Motor* pMotor;
  };
  
}


#endif // STEP_H