#ifndef GPIO_H
#define GPIO_H

#include <cstdint>

namespace wms 
{
  using std::uint32_t;
  
  class Pin
  {
  public:
    enum PinNumber {PIN16 = 16, PIN17, PIN18, PIN19, PIN20, PIN21, PIN22, PIN23};
    enum Port      {PORT0 = 0xE0028000, PORT1 = 0xE0028010};
    enum Direction {IN, OUT};
    
    Pin(Port p, PinNumber n, Direction d);
    void set();
    void clear();
    bool isSet() const;
    
    Pin& operator=(uint32_t rhs);
    Pin& operator=(const Pin& rhs);
    operator int();
    
  private:
    struct Registers
    {
      uint32_t PIN;
      uint32_t SET;
      uint32_t DIR;
      uint32_t CLR;
    };
    
    volatile Registers& registers;
    PinNumber num;
  };
}
  
  
#endif // GPIO_H