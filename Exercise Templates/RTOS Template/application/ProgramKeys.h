#ifndef PROGRAMKEYS_H
#define PROGRAMKEYS_H

#include "Pin.h"
#include <cstdint>

namespace wms 
{
  using std::uint32_t;
  
  class ProgramKeys
  {
  public:
    ProgramKeys();
    uint32_t getSelection();
    
  private:
    Pin latch;
    Pin accept;
    Pin cancel;
    Pin PS1;
    Pin PS2;
    Pin PS3;
  };
}

#endif // PROGRAMKEYS_H