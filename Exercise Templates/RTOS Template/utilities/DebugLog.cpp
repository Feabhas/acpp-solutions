#include "DebugLog.h"
#include <cstdio>
#include <cstdio>
#include <cstring>

// Global debug object.
//
DebugLog dout;

DebugLog::DebugLog()
{
  // The radix is a format string
  // for use with sprintf.
  //
  radix[0] = '%';
  radix[1] = 'd';
  radix[2] = '\0';
}


DebugLog::~DebugLog() 
{
}

DebugLog& DebugLog::operator<< (const char* str)
{
  if (str != NULL)
  {
    for(int i = 0; i < std::strlen(str); ++i)
    std::putchar(str[i]);
  }
  return *this;
}

DebugLog& DebugLog::operator<< (const DebugLog::base& b)
{
  radix[1] = (b == hex)? 'x' : 'd';
  return *this;
}

DebugLog& DebugLog::operator<< (const std::uint32_t& value)
{
  char buffer[(sizeof(std::uint32_t) * 8) + 1];
  std::sprintf(buffer, radix, value);
  operator<<(buffer);
  return *this;
}

DebugLog& DebugLog::operator<< (DebugLog& (*pf)(DebugLog& os))
{
  return pf(*this);
}

DebugLog& endl(DebugLog& os)
{
  os << "\n";
  return os;
}