#include "Pin.h"

Pin::Pin(Port p, PinNumber n, Direction d) :
  registers(*(reinterpret_cast<Registers*>(p))),
  num(n)
{
  registers.DIR |= (d << num);
  registers.CLR  = (1 << num);   
}

void Pin::set()
{
  registers.SET = (1 << num);
}

void Pin::clear()
{
  registers.CLR = (1 << num);
}

bool Pin::isSet() const
{
  return ((registers.PIN & (1 << num)) != 0);
}

