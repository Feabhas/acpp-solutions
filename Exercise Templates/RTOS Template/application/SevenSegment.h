#ifndef SEVENSEGMENT_H
#define SEVENSEGMENT_H

#include "Pin.h"
#include "UI_Interfaces.h"
#include <cstdint>

namespace wms 
{
  using std::uint32_t;
  
  class SevenSegment : public I_Output
  {
  public:
    SevenSegment();
    virtual void display(uint32_t value);
    
  private:
    Pin a;
    Pin b;
    Pin c;
    Pin d;
  };
}

#endif // SEVENSEGMENT_H