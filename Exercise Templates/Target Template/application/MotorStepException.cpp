#include "MotorStepException.h"
#include "OpticalSensor.h"
#include "MotorFailed.h"

void MotorStepException::checkMotorIsRunning()
{
  bool currentState = feedbackSensor.read();
  
  for (int i = 0; i < 100000; ++i)
  {
    if(feedbackSensor.read() != currentState) {
      return;
    }
  }
  throw MotorFailed(0);
}
