#ifndef SEVENSEGMENT_H
#define SEVENSEGMENT_H

#include "Pin.h"

using namespace std;

class SevenSegment
{
public:
  SevenSegment();
  void display(uint32_t value);

private:
  Pin a;
  Pin b;
  Pin c;
  Pin d;
};

#endif // SEVENSEGMENT_H