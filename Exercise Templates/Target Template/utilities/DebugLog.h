#ifndef DEBUGLOG_H
#define DEBUGLOG_H

#include <cstdint>

class DebugLog
{
public:
  enum base {dec = 10, hex = 16};
  
  DebugLog();
  ~DebugLog();

  DebugLog& operator<< (const char* str);
  DebugLog& operator<< (const base& b);
  DebugLog& operator<< (const std::uint32_t& value);
  DebugLog& operator<< (DebugLog& (*pf)(DebugLog& os));
  
  void endl();
  
private:
  
  // Disable copying
  //
  DebugLog(const DebugLog&);
  DebugLog& operator= (const DebugLog& rhs);
  
  // Radix for sprintf conversion
  char radix[3];
};

DebugLog& endl(DebugLog& os);

extern DebugLog debug;

#endif