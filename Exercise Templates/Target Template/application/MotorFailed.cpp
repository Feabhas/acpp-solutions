#include "MotorFailed.h"

static const char* errorMessage = "Motor Failed to Start\n\r";

const char * MotorFailed::what() const
{
  return errorMessage;
}