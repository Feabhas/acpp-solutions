// -----------------------------------------------------------------------------
// INTERRUPTS
//
// Copyright Feabhas Ltd 2008
// -----------------------------------------------------------------------------

// Timer.h
// Declaration of Timer class

#ifndef TIMER_H
#define TIMER_H

#include <stdint.h>

class Timer
{
public:
  Timer (uint32_t address, uint32_t ms);
  void start();
  void stop();
    
  __irq __arm static void ISR();
    
private:
  struct regs
  {
    uint32_t IR;
    uint32_t TCR;
    uint32_t TC; 
    uint32_t PR;  
    uint32_t PC; 
    uint32_t MCR;
    uint32_t MR0;
    uint32_t MR1;
    uint32_t MR2;
    uint32_t MR3;
    uint32_t CCR;
    uint32_t CR0;
    uint32_t CR1;
    uint32_t CR2;
    uint32_t CR3;
    uint32_t EMR;
  };
    
  volatile regs* const pRegs;
  static Timer* stp;
};
  

#endif // TIMER_H

