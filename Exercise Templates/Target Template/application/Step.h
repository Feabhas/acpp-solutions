#ifndef STEP_H
#define STEP_H

class SevenSegment;

class Step
{
public:
  enum StepType
  {
    EMPTY,
    FILL,
    HEAT,
    WASH,
    RINSE,
    SPIN,
    DRY,
    COMPLETE
  };
  
  Step(StepType step, SevenSegment& disp);
  virtual void run();

private:
  SevenSegment& pDisplay;
  StepType stepNumber; 
};



#endif // STEP_H