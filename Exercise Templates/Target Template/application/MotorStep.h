#ifndef MOTOR_STEP_H
#define MOTOR_STEP_H
// ---------------------------------------------------------
// ABC for motor-based steps.
//
#include "motor.h"
#include "Step.h"

class MotorStep : public Step
{
public:
  MotorStep(StepType step, SevenSegment& disp, Motor& m) : Step(step, disp), motor(m) {}
  virtual ~MotorStep() {}
  virtual void run() = 0;
protected:
  Motor& motor;
};

#endif

