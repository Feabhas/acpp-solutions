#include "SpinStep.h"
#include "wait.h"

SpinStep::SpinStep(StepType step, SevenSegment& disp, Motor& m) : 
  MotorStep(step, disp, m)
{
}


void SpinStep::run()
{
  Step::run();  // Display value on SevenSegment
  motor.on();
  wait();
  wait();
  motor.off();
  motor.changeDirection();
}
