#include "Step.h"
#include "SevenSegment.h"
#include "Motor.h"
#include "UI_Interfaces.h"
#include <algorithm>

namespace wms 
{
  using std::fill;
  using std::vector;
  
  static void wait()
  {
    for (int i = 0; i < 5000000; ++i)
    {
      ; // waste time...
    }
  }
  
  
  Step::Step(StepType step, I_Output* output) : 
    pOutput(output),
    stepNumber(step)
  {
  }
  
  void Step::run()
  {
    pOutput->display(stepNumber + 1);
    wait();
  }
  
  // ---------------------------------------------------------
  
  WashStep::WashStep(StepType step, I_Output* output, Motor* m) : 
    MotorStep(step, output, m)
  {
  }
  
  
  void WashStep::run()
  {
    Step::run();  // Display value on SevenSegment
    motor->on();
    wait();
    motor->changeDirection();
    wait();
    motor->off();
  }
  
  
  SpinStep::SpinStep(StepType step, I_Output* output, Motor* m) : 
    MotorStep(step, output, m)
  {
  }
  
  
  void SpinStep::run()
  {
    Step::run();  // Display value on SevenSegment
    motor->on();
    wait();
    wait();
    motor->off();
  }
  
  // ---------------------------------------------------------
  
  StepFactory::StepFactory(I_Output* output, Motor* motor) :
    stepContainer(NUMBER_OF_STEPS),
    pOutput(output),
    pMotor(motor)
  {
    fill(stepContainer.begin(), stepContainer.end(), static_cast<Step*>(0));
  }
  
  
  Step* StepFactory::make(StepType step)
  {
    switch(step)
    {
    case WASH:
      if (stepContainer[step] == 0)
      {
        stepContainer[step] = new WashStep(step, pOutput, pMotor);
      }
      break;
      
    case SPIN:
      if (stepContainer[step] == 0)
      {
        stepContainer[step] = new SpinStep(step, pOutput, pMotor);
      }
      break;
      
    default:  // All other steps...
      if (stepContainer[step] == 0)
      {
        stepContainer[step] = new Step(step, pOutput);
      }
      break;
    }
    
    return stepContainer[step];
  }
  
  
  StepFactory::~StepFactory()
  {
    for (int i = 0; i < NUMBER_OF_STEPS; ++i)
    {
      delete stepContainer[i];
    }
  }
  
}
