#include "ProgramKeys.h"

namespace wms 
{
  ProgramKeys::ProgramKeys() :
    latch(Pin::PORT1, Pin::PIN22, Pin::OUT),
    accept(Pin::PORT0, Pin::PIN21, Pin::IN),
    cancel(Pin::PORT0, Pin::PIN20, Pin::IN),
    PS1(Pin::PORT0, Pin::PIN17, Pin::IN),
    PS2(Pin::PORT0, Pin::PIN18, Pin::IN),
    PS3(Pin::PORT0, Pin::PIN19, Pin::IN)
  {
    latch = 0;
  }
  
  uint32_t ProgramKeys::getSelection()
  {
    uint32_t programSelection = 0;
  
    latch = 1;
    while(!accept)
    {
      if(PS1 == 1) programSelection |= 1;
      if(PS2 == 1) programSelection |= 2;
      if(PS3 == 1) programSelection |= 4;
      
      if(cancel.isSet())
      {
        latch = 0;
        programSelection = 0;
        latch = 1;
      }
    }
    latch = 0;
   
    return programSelection;
  }
}