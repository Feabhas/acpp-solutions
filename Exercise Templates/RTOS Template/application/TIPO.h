// TIPO.h
//
// Header for Thread-Is-Polymorphic-Object

#ifndef TIPO_H
#define TIPO_H

#include "FreeRTOS.h"
#include "Task.h"
#include <cstdint>

//-----------------------------------------------------------------
// Thread configuration.
// Based on FreeRTOS.
//

// Thread Abstract Base class.  All working threads must
// inherit from this class and implement the pure virtual
// run() function.
//
class Thread
{
public:
  enum Priority 
  {
    LowestPriority  = tskIDLE_PRIORITY + 1,
    LowPriority     = tskIDLE_PRIORITY + 2,
    NormalPriority  = tskIDLE_PRIORITY + 3,
    HighPriority    = tskIDLE_PRIORITY + 4,
    HighestPriority = tskIDLE_PRIORITY + 5
  };

  enum Stack
  {
    DefaultStack = 100,
    TinyStack    = 32,
    SmallStack   = 256,
    LargeStack   = 2048,
    HugeStack    = 4096
  };
  
  explicit Thread (Priority prio);
  virtual ~Thread ();
  
  void start ();
  
protected:
  virtual int run() = 0;
  
private:
  Thread (const Thread&);
  Thread& operator= (const Thread&);
 
  // --------------------------------
  // FreeRTOS-specific data:
  //
  Stack       stack;
  Priority    priority;
  xTaskHandle handle;
  // --------------------------------
  
  bool created;
  static void scheduledFunction (void* threadArg);
};



#endif // TIPO_H
