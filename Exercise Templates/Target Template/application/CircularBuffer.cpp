#include "CircularBuffer.h"

CircularBuffer::CircularBuffer() :
  write(0),
  read(0),
  numItems(0)
{
}

CircularBuffer::Error CircularBuffer::add(int value)
{
  Error err = FULL;
  
  if (numItems != buffer_size)
  {
    buffer[write] = value;
    ++numItems;
    ++write;
    if (write == buffer_size) write = 0;
    err = OK;
  }
  return err;
}

CircularBuffer::Error CircularBuffer::get(int& value)
{
  Error err = EMPTY;
  
  if (numItems != 0)
  {
    value = buffer[read];
    --numItems;
    ++read;
    if (read == buffer_size) read = 0;
    err = OK;
  }
  return err;
}

bool CircularBuffer::isEmpty()
{
  return (numItems == 0);
}
