#ifndef SYSTEM_TASKS_H
#define SYSTEM_TASKS_H

//  Create a new UI class that represents the user interface of 
//  the system.  The UI class should realise the IRunnable interface.
//
//  Create your UI class and connect it to the UART class so that 
//  the user can select wash programmes and receive output via a 
//  console display.
//
//  Run the UI class in its own thread of control (task).
//
//
//  Your UI class should allow the user to select a wash programme.  The 
//  selected wash programme should be sent to the WMS class to run. 
//
//  Your design should allow asynchronous communication between the UI 
//  and WMS tasks.  Use your CircularBuffer to facilitate communication.


#endif // SYSTEM_TASKS_H